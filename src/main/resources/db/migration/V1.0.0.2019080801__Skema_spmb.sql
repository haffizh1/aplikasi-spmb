-- SKEMA SECURITY
CREATE TABLE s_permission (
  id               VARCHAR(255) NOT NULL,
  permission_label VARCHAR(255) NOT NULL,
  permission_value VARCHAR(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (permission_value)
);

CREATE TABLE s_role (
  id          VARCHAR(255) NOT NULL,
  description VARCHAR(255) DEFAULT NULL,
  name        VARCHAR(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (name)
);

CREATE TABLE s_role_permission (
  id_role       VARCHAR(255) NOT NULL,
  id_permission VARCHAR(255) NOT NULL,
  PRIMARY KEY (id_role, id_permission),
  FOREIGN KEY (id_permission) REFERENCES s_permission (id),
  FOREIGN KEY (id_role) REFERENCES s_role (id)
);

CREATE TABLE s_user (
  id       VARCHAR(36),
  username VARCHAR(255) NOT NULL,
  active   BOOLEAN      NOT NULL,
  id_role  VARCHAR(255) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id_role) REFERENCES s_role (id)
);

create table s_user_password (
  id varchar(36),
  id_user varchar(36) not null,
  password varchar(255) not null,
  primary key (id),
  foreign key (id_user) references s_user (id)
);

CREATE TABLE reset_password (
  id VARCHAR(36) NOT NULL ,
  id_user VARCHAR(36) NOT NULL ,
  code VARCHAR(36) NOT NULL ,
  expired DATE NOT NULL ,
  PRIMARY KEY (id),
  FOREIGN KEY (id_user) REFERENCES s_user(id)
);

--
-- SKEMA PENDAFTARAN
CREATE TABLE provinsi(
  id  VARCHAR (36),
  nama VARCHAR (255),
  PRIMARY KEY (id)
);

CREATE TABLE kabupaten_kota(
  id  VARCHAR (36),
  nama VARCHAR (255),
  id_provinsi VARCHAR (36),
  PRIMARY KEY (id),
  FOREIGN KEY (id_provinsi) REFERENCES provinsi (id)
);

CREATE TABLE tahun_ajaran(
  id                      VARCHAR (36),
  nama                    VARCHAR (255) NOT NULL,
  aktif                   VARCHAR (36),
  PRIMARY KEY (id)
);

CREATE  TABLE  leads(
  id                      VARCHAR (36),
  nama                    VARCHAR (255) NOT NULL,
  email                   VARCHAR (255) NOT NULL,
  no_hp                   VARCHAR (36) NOT NULL,
  jenjang                 VARCHAR (36) NOT null,
  id_user                 VARCHAR (36),
  id_tahun                VARCHAR (36),
  PRIMARY KEY (id),
  FOREIGN KEY (id_user) REFERENCES s_user(id),
  FOREIGN KEY (id_tahun) REFERENCES tahun_ajaran(id)
);

CREATE TABLE pendaftar (
  id                      VARCHAR (36),
  id_leads                VARCHAR (36),
  nomor_registrasi        VARCHAR (255) NOT NULL,
  id_program_studi        VARCHAR (36)  NOT NULL,
  konsentrasi             VARCHAR (255),
  nama_asal_sekolah       VARCHAR (255) NOT NULL,
  kota_asal_sekolah         VARCHAR (255) NOT NULL,
  perekomendasi           VARCHAR (255) NOT NULL,
  nama_perekomendasi      VARCHAR (255),
  id_tahun                VARCHAR (255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (nomor_registrasi),
  FOREIGN KEY (id_leads) REFERENCES leads(id),
  FOREIGN KEY (id_tahun) REFERENCES tahun_ajaran(id)

);

CREATE TABLE pendaftar_detail (
  id                      VARCHAR (36),
  id_pendaftar            VARCHAR (36) NOT  NULL,
  foto                    VARCHAR (255) NOT NULL,
  tempat_lahir            VARCHAR (255) NOT NULL,
  tanggal_lahir           DATE,
  agama                   VARCHAR (255) NOT NULL,
  jenis_kelamin           VARCHAR (36) NOT NULL,
  golongan_darah          VARCHAR (36) NOT NULL,
  no_ktp                  VARCHAR (255) NOT NULL,
  alamat_rumah            VARCHAR (255) NOT NULL,
  negara                  VARCHAR (255) NOT NULL,
  id_provinsi             VARCHAR (36) NOT NULL,
  id_kokab                VARCHAR (36) NOT NULL,
  kode_pos                varchar (36) NOT NULL,
  nisn                    VARCHAR (36) NOT NULL,
  jurusan_sekolah         VARCHAR (36) NOT NULL,
  status_sipil            VARCHAR (36) NOT NULL,
  tahun_lulus             VARCHAR (36) NOT NULL,
  rencana_biaya           VARCHAR (36) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (no_ktp,nisn),
  FOREIGN KEY (id_pendaftar) REFERENCES pendaftar (id)

);

CREATE TABLE pendaftar_orangtua(
  id                      VARCHAR (36),
  id_pendaftar            VARCHAR (36),
  nama_ayah               VARCHAR (255) NOT NULL,
  agama_ayah              VARCHAR (255) NOT NULL,
  pendidikan_ayah         VARCHAR (255) NOT NULL,
  pekerjaan_ayah          VARCHAR (255) NOT NULL,
  status_ayah             VARCHAR (36)  NOT NULL,
  nama_ibu                VARCHAR (255) NOT NULL,
  agama_ibu               VARCHAR (255) NOT NULL,
  pendidikan_ibu          VARCHAR (255) NOT NULL,
  pekerjaan_ibu           VARCHAR (255) NOT NULL,
  status_ibu              VARCHAR (36)  NOT NULL,
  alamat                  VARCHAR (255) NOT NULL,
  no_orangtua             VARCHAR (255) NOT NULL,
  penghasilan             VARCHAR (255) NOT NULL,
  jumlah_tanggungan       VARCHAR (255) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id_pendaftar) REFERENCES pendaftar(id)
);

CREATE TABLE jadwal_test (
  id VARCHAR (36),
  id_pendaftar VARCHAR(36) NOT NULL,
  jenis_test VARCHAR (36) NOT NULL,
  tanggal_test DATE,
  PRIMARY KEY (id),
  FOREIGN KEY (id_pendaftar) REFERENCES pendaftar(id)
);

CREATE TABLE hasil_test(
  id                      VARCHAR (36),
  id_jadwal               VARCHAR (36),
  nilai                   NUMERIC(5,2) not NULL default 0,
  id_grade                VARCHAR (36) NOT NULL,
  id_periode              VARCHAR (36) NOT NULL,
  user_insert             VARCHAR (36) NOT NULL,
  tanggal_insert          timestamp,
  PRIMARY KEY (id),
  FOREIGN KEY (id_jadwal) REFERENCES jadwal_test(id)
);

CREATE TABLE berkas (
  id VARCHAR (36),
  id_pendaftar VARCHAR(36) NOT NULL,
  jenis_berkas VARCHAR (36) NOT NULL,
  file_berkas VARCHAR (255) NOT NULL ,
  PRIMARY KEY (id),
  FOREIGN KEY (id_pendaftar) REFERENCES pendaftar(id)
);

CREATE TABLE sekolah (
  id                VARCHAR (36),
  nama              VARCHAR(255) NOT NULL,
  alamat            VARCHAR(255) NOT NULL,
  kontak            VARCHAR(255),
  nspn              VARCHAR(255),
  PRIMARY KEY (id)
);

CREATE TABLE program_studi(
  id          VARCHAR (36),
  nama        VARCHAR (255),
  kode_biaya  VARCHAR (36),
  kode_simak  VARCHAR (36),
  PRIMARY KEY (id)
);

CREATE TABLE periode(
  id VARCHAR (36),
  nama VARCHAR (255),
  tanggal_mulai DATE,
  tanggal_selesai DATE,
  PRIMARY KEY (id)
);

CREATE TABLE grade(
  id VARCHAR (36),
  nama VARCHAR (255),
  nilai_minimum NUMERIC(5,2) not NULL default 0,
  PRIMARY KEY (id)
);

CREATE TABLE running_number (
id  VARCHAR (36),
nama  VARCHAR(255) NOT NULL ,
nomer_terakhir BIGINT NOT NULL,
UNIQUE (nama)
 );

CREATE TABLE pekerjaan (
  id    VARCHAR (36),
  nama  VARCHAR(36) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE pendidikan (
  id    VARCHAR (36),
  nama  VARCHAR(36) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE penghasilan (
  id    VARCHAR (36),
  nama  VARCHAR(36) NOT NULL,
  PRIMARY KEY (id)
);

-- SKEMA TAGIHAN & PEMBAYARAN

CREATE TABLE jenis_biaya(
  id VARCHAR (36),
  nama VARCHAR (255),
  PRIMARY KEY (id)
);

CREATE TABLE nilai_biaya(
  id VARCHAR (36),
  id_jenisbiaya VARCHAR (255),
  id_grade VARCHAR (255),
  id_periode VARCHAR (255),
  id_programstudi VARCHAR (255),
  nilai numeric(19,2) NOT NULL DEFAULT 0,
  tanggal_edit date,
  user_edit VARCHAR (36),
  PRIMARY KEY (id)
);

create table tagihan (
  id VARCHAR (36),
  id_pendaftar VARCHAR (36) NOT NULL,
  id_jenisbiaya VARCHAR (36) NOT NULL,
  nomor_tagihan VARCHAR (255) NOT NULL,
  tanggal_tagihan DATE NOT NULL,
  keterangan VARCHAR (255) NOT NULL,
  nilai NUMERIC (19,2) NOT NULL,
  lunas BOOLEAN NOT NULL DEFAULT FALSE,
  PRIMARY KEY (id),
  UNIQUE (nomor_tagihan),
  FOREIGN KEY (id_pendaftar) REFERENCES pendaftar(id),
  FOREIGN KEY (id_jenisbiaya) REFERENCES jenis_biaya(id)
);

CREATE TABLE bank (
  id        VARCHAR(36),
  kode_bank VARCHAR(100) NOT NULL,
  nama_bank VARCHAR(255) NOT NULL,
  nomor_rekening VARCHAR(255) NOT NULL,
  nama_rekening VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);

create table virtual_account(
  id varchar (36),
  id_bank varchar (36),
  id_tagihan varchar (36),
  nomor_va varchar (36),
  PRIMARY KEY (id),
  FOREIGN KEY (id_bank) REFERENCES bank (id),
  FOREIGN KEY (id_tagihan) REFERENCES tagihan (id)
);

create table pembayaran (
  id VARCHAR (36),
  id_tagihan VARCHAR(36) NOT NULL,
  id_bank VARCHAR (36) NOT NULL,
  waktu_pembayaran TIMESTAMP NOT NULL ,
  cara_pembayaran VARCHAR (255) NOT NULL ,
  bukti_pembayaran VARCHAR (255) NOT NULL ,
  referensi VARCHAR (255) NOT NULL ,
  nilai NUMERIC(19,2) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id_tagihan) REFERENCES tagihan(id),
  FOREIGN KEY (id_bank) REFERENCES bank(id)
);

