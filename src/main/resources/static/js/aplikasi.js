$(document).ready(function(){
    var urlProvinsi = "/api/provinsi";

    var urlSekolah ="/api/sekolah";
    var urlKokabawal = "/api/kokabawal";

    var templateUrlKabupaten = "/api/provinsi/{provinsi}/kabupaten";

    var urlKabupaten = null;

    var sekolah = null;
    var kokabawal = null;
    var provinsi = null;
    var kabupatenKota = null;


    var inputSekolah = $("#sekolah");
    var inputKokabawal = $("#kokabawal");
    var inputHiddenKokab = $("input[name=idKabupatenKota]");
    var inputTempatLahir = $("#tempatLahir");

    var inputProvinsi = $("#provinsi");
    var inputIdProvinsi = $("#idProvinsi")
    var inputKabupatenKota = $("#kabupatenKota");
    var inputIdKabupatenKota = $("#idKabupatenKota");





    var resetInput = function(inputField){
        inputField.val('');
        inputField.prop('disabled', true);
    };

    inputSekolah.typeahead({
        displayText: function(item){ return item.nama;},
        source: _.debounce(function(cari, process){
            sekolah = null;
            $.get(urlSekolah, {nama: cari}, function(hasil){
                process(hasil.content);
            }, "json");
            console.log(inputSekolah.val())
        }, 500)

    });

    inputKokabawal.typeahead({
        displayText: function(item){ return item.nama;},
        source: _.debounce(function(cari, process){
            kokabawal = null;
            $.get(urlKokabawal, {nama: cari}, function(hasil){
                process(hasil);
            }, "json");
        }, 500),
        afterSelect: function(pilihan) {
            inputHiddenKokab.val(pilihan.id);
        }
    });


    inputTempatLahir.typeahead({
        displayText: function(item){ return item.nama;},
        source: _.debounce(function(cari, process){
            kokabawal = null;
            $.get(urlKokabawal, {nama: cari}, function(hasil){
                process(hasil);
            }, "json");
        }, 500)
    });



    inputProvinsi.typeahead({
        displayText: function(item){ return item.nama;},
        source: _.debounce(function(cari, process){
            provinsi = null;
            resetInput(inputKabupatenKota);
            $.get(urlProvinsi, {nama: cari}, function(hasil){
                process(hasil);
            }, "json");
        }, 500),
        afterSelect: function(pilihan){
            provinsi = pilihan;
            inputKabupatenKota.prop('disabled', false);
            urlKabupaten = _.replace(templateUrlKabupaten, '{provinsi}', provinsi.id);
            inputIdProvinsi.val(pilihan.id);
        }
    });

    inputKabupatenKota.typeahead({
        displayText: function(item){ return item.nama;},
        source: _.debounce(function(cari, process){
            kabupatenKota = null;
            $.get(urlKabupaten, {nama: cari}, function(hasil){
                process(hasil);
            }, "json");
        }, 500),
        afterSelect: function(pilihan) {
            console.log( pilihan.id);
            inputIdKabupatenKota.val(pilihan.id);
        }
    });

});