package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import sun.security.util.Pem;

import java.time.LocalDateTime;
import java.util.List;

public interface PembayaranDao extends PagingAndSortingRepository<Pembayaran, String> {
    Pembayaran findByTagihan(Tagihan tagihan);
    Page<Pembayaran> findByTagihan(Tagihan tagihan, Pageable pageable);

    Page<Pembayaran> findByTagihanJenisBiayaAndTagihanPendaftarLeadsNamaContainingIgnoreCaseAndTagihanPendaftarTahunAjaran(JenisBiaya jenisBiaya, String pendaftar, TahunAjaran tahunAjaran,Pageable page);
    Page<Pembayaran> findByTagihanJenisBiayaAndTagihanPendaftarTahunAjaran(JenisBiaya jenisBiaya, TahunAjaran tahunAjaran ,Pageable page);
    Page<Pembayaran> findByTagihanPendaftarLeadsNamaContainingIgnoreCaseAndTagihanPendaftarTahunAjaran(String pendaftar, TahunAjaran tahunAjaran, Pageable page);
    Page<Pembayaran> findByTagihanPendaftarTahunAjaran(TahunAjaran tahunAjaran, Pageable pageable);
    Page<Pembayaran> findByTagihanPendaftarLeadsJenjangAndTagihanPendaftarLeadsNamaContainingIgnoreCase(Jenjang jenjang,String pendaftar, Pageable page);
    Page<Pembayaran> findByTagihanPendaftarLeadsJenjang(Jenjang jenjang, Pageable page);

    Iterable<Pembayaran> findByTagihanJenisBiayaAndWaktuPembayaranBetweenOrderByWaktuPembayaran(JenisBiaya jenis, LocalDateTime localDateTime, LocalDateTime localDateTime1);

    Iterable<Pembayaran> findByTagihanJenisBiaya(JenisBiaya jb);

    @Query(value = "select * from\n" +
            "(select t.id, p.id_tagihan, p.id_bank, p.bukti_pembayaran, p.waktu_pembayaran, p.cara_pembayaran, p.nilai, p.referensi from tagihan t \n" +
            "inner join pembayaran p on p.id_tagihan = t.id\n" +
            "inner join pendaftar pd on pd.id = t.id_pendaftar\n" +
            "inner join leads l on l.id = pd.id_leads\n" +
            "inner join  bank b on b.id = p.id_bank\n" +
            "where t.id_jenisbiaya = 002  and t.lunas = true and pd.id_tahun = :tahun and l.jenjang = 'S1' \n" +
            "group by t.id_pendaftar) aa", nativeQuery = true)
    Iterable<Pembayaran>findByTagihanS1One(TahunAjaran tahun);

    @Query(value = "select * from\n" +
            "(select t.id, p.id_tagihan, p.id_bank, p.bukti_pembayaran, p.waktu_pembayaran, p.cara_pembayaran, p.nilai, p.referensi from tagihan t \n" +
            "inner join pembayaran p on p.id_tagihan = t.id\n" +
            "inner join pendaftar pd on pd.id = t.id_pendaftar\n" +
            "inner join leads l on l.id = pd.id_leads\n" +
            "inner join  bank b on b.id = p.id_bank\n" +
            "where t.id_jenisbiaya = 002  and t.lunas = true and pd.id_tahun = :tahun and l.jenjang = 'S2' \n" +
            "group by t.id_pendaftar) aa", nativeQuery = true)
    Iterable<Pembayaran>findByTagihanS2One(TahunAjaran tahun);


    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaran(JenisBiaya jenisBiaya, TahunAjaran ta);
    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjang(JenisBiaya jenisBiaya, TahunAjaran ta, Jenjang j);
    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudi(JenisBiaya jenisBiaya, TahunAjaran ta, Jenjang j, ProgramStudi prodi);

    @Query(value="select count(*) as jumlah from  (select t.id from tagihan t " +
            "inner join pendaftar p on p.id = t.id_pendaftar" +
            " inner join jadwal_test j on j.id_pendaftar = p.id " +
            " inner join hasil_test h on h.id_jadwal = j.id " +
            " inner join leads l on l.id = p.id_leads where t.id_jenisbiaya = 002  and  p.id_tahun = :tahun " +
            "and l.jenjang = :jenjang and t.lunas = true  group by t.id_pendaftar) aa", nativeQuery = true)
    Long hitungDUJen(TahunAjaran tahun, String jenjang);

    @Query(value = "select count(*) as jumlah from  (select t.id from tagihan t inner join pendaftar p on p.id = t.id_pendaftar " +
            " inner join jadwal_test j on j.id_pendaftar = p.id " +
            " inner join hasil_test h on h.id_jadwal = j.id " +
            " inner join leads l on l.id = p.id_leads where t.id_jenisbiaya = 001  and  p.id_tahun = :tahun " +
            " and l.jenjang = :jenjang and t.lunas = true   group by t.id_pendaftar) aa", nativeQuery = true)
    Long hitungYangSudahTest(TahunAjaran tahun, String jenjang);

    @Query(value="select count(*) as jumlah from  (select t.id from tagihan t " +
            "inner join pendaftar p on p.id = t.id_pendaftar" +
            " inner join jadwal_test j on j.id_pendaftar = p.id " +
            " inner join hasil_test h on h.id_jadwal = j.id " +
            " inner join leads l on l.id = p.id_leads where t.id_jenisbiaya = 002  and  p.id_tahun = :tahun" +
            " and p.id_program_studi = :prodi  " +
            " and t.lunas = true  group by t.id_pendaftar) aa", nativeQuery = true)
    Long hitungDUJenisTest(TahunAjaran tahun,  String prodi);

    @Query(value="select count(*) as jumlah from  (select t.id from tagihan t " +
            "inner join pendaftar p on p.id = t.id_pendaftar" +
            " inner join jadwal_test j on j.id_pendaftar = p.id " +
            " inner join hasil_test h on h.id_jadwal = j.id " +
            " inner join leads l on l.id = p.id_leads where t.id_jenisbiaya = 002  and  p.id_tahun = :tahun" +
            " and t.lunas = true  group by t.id_pendaftar) aa", nativeQuery = true)
    Long hitungDUTotal(TahunAjaran tahun);

    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(JenisBiaya jb, TahunAjaran ta, String pe, ProgramStudi pr);
    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarLeadsJenjang(JenisBiaya jb, TahunAjaran ta, String pe, Jenjang jenjang);
    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelamin(JenisBiaya jb, TahunAjaran ta, String pe);


    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(JenisBiaya jb, TahunAjaran ta,ProgramStudi pr, JenisTest jenisTest);
    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarJadwalTestJenisTest(JenisBiaya jb, TahunAjaran ta, JenisTest jenisTest);
    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarJadwalTestJenisTest(JenisBiaya jb, TahunAjaran ta,Jenjang jenjang, JenisTest jenisTest);

    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudi(JenisBiaya jb, TahunAjaran ta,ProgramStudi pr);


    Page<Pembayaran> findByTagihanPendaftar(Pendaftar pendaftar, Pageable pageable);


    @Query(value = "select p.nomor_registrasi,l.nama,l.email,l.no_hp,pr.nama as npprodi,t.cicilan, SUM(pem.nilai) as nilai,coalesce(t.total_tagihan,'-'), h.keterangan from pembayaran pem\n" +
            "inner join tagihan t on t.id = pem.id_tagihan\n" +
            "inner join pendaftar p on p.id = t.id_pendaftar\n" +
            "inner join program_studi pr on pr.id = p.id_program_studi \n" +
            "inner join leads l on l.id = p.id_leads\n" +
            "inner join jadwal_test j on j.id_pendaftar = p.id\n" +
            "inner join hasil_test h on h.id_jadwal = j.id\n" +
            "where p.id_tahun = :tahun and t.id_jenisbiaya = '002' group by p.nomor_registrasi", nativeQuery = true)
    Iterable<Object[]>findByAllPembayaranDU(TahunAjaran tahun);
}
