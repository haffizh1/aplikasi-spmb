package id.ac.tazkia.spmb.aplikasispmb.dto;

import lombok.Data;

@Data
public class NotifRegistrasiReferalDto {
    private String nomor;
    private String nama;
    private String email;
    private String noHp;
    private String emailRef;
    private String kodeRef;
}
