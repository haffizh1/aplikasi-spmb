package id.ac.tazkia.spmb.aplikasispmb.dto;

import id.ac.tazkia.spmb.aplikasispmb.entity.Bank;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class VaResponse {
    private String accountNumber;
    private String invoiceNumber;
    private String name;
    private BigDecimal amount;
    private String bankId;
}
