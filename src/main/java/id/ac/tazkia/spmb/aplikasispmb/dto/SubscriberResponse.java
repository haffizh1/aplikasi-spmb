package id.ac.tazkia.spmb.aplikasispmb.dto;

import lombok.Data;

@Data
public class SubscriberResponse {
    private String error_code;
    private String error_desc;
}
