package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class DashboardController {
    private static final Logger logger = LoggerFactory.getLogger(DashboardController.class);

    @Autowired private UserDao userDao;
    @Autowired private RoleDao roleDao;
    @Autowired private TahunAjaranDao tahunAjaranDao;
    @Autowired private PendaftarDao pendaftarDao;
    @Autowired private LeadsDao leadsDao;
    @Autowired private PendaftarDetailDao pendaftarDetailDao;
    @Autowired private PendaftarDetailPascaDao pendaftarDetailPascaDao;
    @Autowired private PembayaranDao pembayaranDao;
    @Autowired private HasilTestDao hasilTestDao;
    @Autowired private ProgramStudiDao programStudiDao;


    @GetMapping("/dashboard")
    public String dashboard(Authentication currentUser, Model model, Pageable page){
        logger.debug("Authentication class : {}",currentUser.getClass().getName());

        if(currentUser == null){
            logger.warn("Current user is null");
        }

        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

        Role role = roleDao.findById(AppConstants.ROLE_PENDAFTAR).get();

        if (u.getRole() == role ){
            return "redirect:dashboardPendaftar";
        }else{
            TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
            model.addAttribute("tahunAjaran", ta);
            model.addAttribute("cS1Leads", leadsDao.countLeadsByTahunAjaranAndJenjang(ta, Jenjang.S1));
            model.addAttribute("cS1Pendaftar", pendaftarDao.countPendaftarByLeadsJenjangAndTahunAjaran(Jenjang.S1,ta));
            model.addAttribute("cS1BelumDaftar", leadsDao.countLeadsByTahunAjaranAndJenjang(ta, Jenjang.S1) -   pendaftarDao.countPendaftarByLeadsJenjangAndTahunAjaran(Jenjang.S1,ta));
            model.addAttribute("cS1Detail", pendaftarDetailDao.countByPendaftarTahunAjaran(ta));
            model.addAttribute("cS1BelumDetail", pembayaranDao.hitungDUJen(ta, "S1") - pendaftarDetailDao.countByPendaftarTahunAjaran(ta));

            JenisBiaya jb = new JenisBiaya();
            jb.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            model.addAttribute("cS1Pembayaran", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjang(jb, ta, Jenjang.S1));
            model.addAttribute("cS1BelumBayar", pendaftarDao.countPendaftarByLeadsJenjangAndTahunAjaran(Jenjang.S1,ta) - pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjang(jb, ta, Jenjang.S1));

            model.addAttribute("cS1SudahTest", pembayaranDao.hitungYangSudahTest(ta, "S1"));
            model.addAttribute("cS1BelumTest", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjang(jb, ta, Jenjang.S1) - pembayaranDao.hitungYangSudahTest(ta, "S1"));

            JenisBiaya jbD = new JenisBiaya();
            jbD.setId(AppConstants.JENIS_BIAYA_DAFTAR_ULANG);
            model.addAttribute("cDu1", pembayaranDao.hitungDUJen(ta, "S1"));
            model.addAttribute("cBelumDu1",pembayaranDao.hitungYangSudahTest(ta, "S1")-pembayaranDao.hitungDUJen(ta, "S1"));

            model.addAttribute("cS2Leads", leadsDao.countLeadsByTahunAjaranAndJenjang(ta, Jenjang.S2));
            model.addAttribute("cS2Pendaftar", pendaftarDao.countPendaftarByLeadsJenjangAndTahunAjaran(Jenjang.S2,ta));
            model.addAttribute("cS2BelumDaftar", leadsDao.countLeadsByTahunAjaranAndJenjang(ta, Jenjang.S2) -   pendaftarDao.countPendaftarByLeadsJenjangAndTahunAjaran(Jenjang.S2,ta));
            model.addAttribute("cS2Detail", pendaftarDetailPascaDao.countByPendaftarTahunAjaran(ta));
            model.addAttribute("cS2BelumDetail", pembayaranDao.hitungYangSudahTest(ta, "S2") - pendaftarDetailPascaDao.countByPendaftarTahunAjaran(ta));

            model.addAttribute("cS2Pembayaran", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjang(jb, ta, Jenjang.S2));
            model.addAttribute("cS2BelumBayar", pendaftarDao.countPendaftarByLeadsJenjangAndTahunAjaran(Jenjang.S2,ta) - pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjang(jb, ta, Jenjang.S2));

            model.addAttribute("cS2SudahTest", pembayaranDao.hitungYangSudahTest(ta, "S2"));
            model.addAttribute("cS2BelumTest", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjang(jb, ta, Jenjang.S2) - pembayaranDao.hitungYangSudahTest(ta, "S2"));

            model.addAttribute("cDu2", pembayaranDao.hitungDUJen(ta, "S2"));
            model.addAttribute("cBelumDu2",pendaftarDetailPascaDao.countByPendaftarTahunAjaran(ta) -pembayaranDao.hitungDUJen(ta, "S2"));


            ProgramStudi pr1 = new ProgramStudi();
            pr1.setId("001");
            model.addAttribute("ikhwanDuEs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi
                            (jbD,ta,"PRIA", pr1));
            model.addAttribute("akhwatDuEs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(jbD,ta,"WANITA", pr1));

            model.addAttribute("jpaEs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,pr1,JenisTest.JPA));
            model.addAttribute("tpaEs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,pr1,JenisTest.TPA));
            model.addAttribute("smrtEs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,pr1,JenisTest.SMART_TEST));

            model.addAttribute("jmlhPeEs", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaran(pr1,ta));

            model.addAttribute("regisEs", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudi(jb, ta, Jenjang.S1, pr1));

            model.addAttribute("duEs",pembayaranDao.hitungDUJenisTest(ta,pr1.getId()));

            ProgramStudi pr2 = new ProgramStudi();
            pr2.setId("002");
            model.addAttribute("ikhwanDuAs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(jbD,ta,"PRIA", pr2));
            model.addAttribute("akhwatDuAs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(jbD,ta,"WANITA", pr2));

            model.addAttribute("jpaAs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,pr2,JenisTest.JPA));
            model.addAttribute("tpaAs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,pr2,JenisTest.TPA));
            model.addAttribute("smrtAs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,pr2,JenisTest.SMART_TEST));

            model.addAttribute("jmlhPeAs", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaran(pr2,ta));

            model.addAttribute("regisAs", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudi(jb, ta, Jenjang.S1, pr2));

            model.addAttribute("duAs",pembayaranDao.hitungDUJenisTest(ta,pr2.getId()));

            ProgramStudi pr3 = new ProgramStudi();
            pr3.setId("003");
            model.addAttribute("ikhwanDuMb", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(jbD,ta,"PRIA", pr3));
            model.addAttribute("akhwatDuMb", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(jbD,ta,"WANITA", pr3));

            model.addAttribute("jpaMb", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,pr3,JenisTest.JPA));
            model.addAttribute("tpaMb", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,pr3,JenisTest.TPA));
            model.addAttribute("smrtMb", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,pr3,JenisTest.SMART_TEST));

            model.addAttribute("jmlhPeMb", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaran(pr3,ta));

            model.addAttribute("regisMb", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudi(jb, ta, Jenjang.S1, pr3));

            model.addAttribute("duMb",pembayaranDao.hitungDUJenisTest(ta,pr3.getId()));

            ProgramStudi pr4 = new ProgramStudi();
            pr4.setId("004");
            model.addAttribute("ikhwanDuHu", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(jbD,ta,"PRIA", pr4));
            model.addAttribute("akhwatDuHu", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(jbD,ta,"WANITA", pr4));

            model.addAttribute("jpaHu", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,pr4,JenisTest.JPA));
            model.addAttribute("tpaHu", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,pr4,JenisTest.TPA));
            model.addAttribute("smrtHu", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,pr4,JenisTest.SMART_TEST));

            model.addAttribute("jmlhPeHu", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaran(pr4,ta));

            model.addAttribute("regisHu", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudi(jb, ta, Jenjang.S1, pr4));

            model.addAttribute("duHu",pembayaranDao.hitungDUJenisTest(ta,pr4.getId()));

            ProgramStudi pr7 = new ProgramStudi();
            pr7.setId("007");
            model.addAttribute("ikhwanDuKpi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(jbD,ta,"PRIA", pr7));
            model.addAttribute("akhwatDuKpi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(jbD,ta,"WANITA", pr7));

            model.addAttribute("jpaKpi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,pr7,JenisTest.JPA));
            model.addAttribute("tpaKpi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,pr7,JenisTest.TPA));
            model.addAttribute("smrtKpi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,pr7,JenisTest.SMART_TEST));

            model.addAttribute("jmlhPeKpi", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaran(pr7,ta));

            model.addAttribute("regisKpi", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudi(jb, ta, Jenjang.S1, pr7));

            model.addAttribute("duKpi",pembayaranDao.hitungDUJenisTest(ta,pr7.getId()));

            ProgramStudi pr6 = new ProgramStudi();
            pr6.setId("006");
            model.addAttribute("ikhwanDuPe", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(jbD,ta,"PRIA", pr6));
            model.addAttribute("akhwatDuPe", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(jbD,ta,"WANITA", pr6));

            model.addAttribute("jpaPe", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,pr6,JenisTest.JPA));
            model.addAttribute("tpaPe", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,pr6,JenisTest.TPA));
            model.addAttribute("smrtPe", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,pr6,JenisTest.SMART_TEST));

            model.addAttribute("jmlhPePe", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaran(pr6,ta));

            model.addAttribute("regisPe", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudi(jb, ta, Jenjang.S1, pr6));

            model.addAttribute("duPe",pembayaranDao.hitungDUJenisTest(ta,pr6.getId()));

            ProgramStudi MES = new ProgramStudi();
            MES.setId("008");

            model.addAttribute("ikhwanDuS2", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(jbD,ta,"PRIA", MES));
            model.addAttribute("akhwatDuS2", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(jbD,ta,"WANITA", MES));
            model.addAttribute("jpaS2", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,MES,JenisTest.JPA));
            model.addAttribute("tpaS2", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,MES,JenisTest.TPA));
            model.addAttribute("smrtS2", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,MES,JenisTest.SMART_TEST));

            model.addAttribute("jmlhPeS2", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaran(MES,ta));

            model.addAttribute("regisMes", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudi(jb, ta, Jenjang.S2, MES));

            model.addAttribute("duMes",pembayaranDao.hitungDUJenisTest(ta,MES.getId()));

            ProgramStudi mas = new ProgramStudi();
            mas.setId("009");

            model.addAttribute("ikhwanDuS2AS", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(jbD,ta,"PRIA", mas));
            model.addAttribute("akhwatDuS2AS", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(jbD,ta,"WANITA", mas));
            model.addAttribute("jpaS2AS", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,mas,JenisTest.JPA));
            model.addAttribute("tpaS2AS", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,mas,JenisTest.TPA));
            model.addAttribute("smrtS2AS", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,mas,JenisTest.SMART_TEST));

            model.addAttribute("jmlhPeS2AS", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaran(mas,ta));

            model.addAttribute("regisS2As", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudi(jb, ta, Jenjang.S2, mas));

            model.addAttribute("duS2As",pembayaranDao.hitungDUJenisTest(ta,mas.getId()));

            model.addAttribute("gIkhwanDu", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelamin(jbD,ta,"PRIA"));

            model.addAttribute("gAkhwatDu", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelamin(jbD,ta,"WANITA"));
            model.addAttribute("gJpa", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,JenisTest.JPA));
            model.addAttribute("gTpa", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,JenisTest.TPA));
            model.addAttribute("gSmrt", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,JenisTest.SMART_TEST));
            model.addAttribute("cPendaftar", pendaftarDao.countPendaftarByTahunAjaran(ta));
            model.addAttribute("cRegistrasi", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaran(jb, ta));

            model.addAttribute("du", pembayaranDao.hitungDUTotal(ta));




            return "dashboard";
        }
    }
}
