package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.dao.TahunAjaranDao;
import id.ac.tazkia.spmb.aplikasispmb.entity.Status;
import id.ac.tazkia.spmb.aplikasispmb.entity.TahunAjaran;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class TahunAjaranController {
    private static final Logger logger = LoggerFactory.getLogger(TahunAjaranController.class);


    @Autowired private TahunAjaranDao tahunAjaranDao;

    @GetMapping("/tahunajaran/list")
    public String formTahunAjaran(@RequestParam(value = "id", required = false) String id,
                                  @PageableDefault(direction = Sort.Direction.ASC) Pageable page,
                                  Model m){
        m.addAttribute("tahunAjaranList", tahunAjaranDao.findAll(page));

        //defaultnya, isi dengan object baru
        m.addAttribute("tahunAjaran", new TahunAjaran());

        if (id != null && !id.isEmpty()){
            TahunAjaran tahunAjaran= tahunAjaranDao.findById(id).get();
            if (tahunAjaran != null){
                m.addAttribute("tahunAjaran", tahunAjaran);
            }

        }
        return "tahunajaran/list";
    }

    @RequestMapping(value = "/tahunajaran/list", method = RequestMethod.POST)
    public String prosesForm(@Valid TahunAjaran tahunAjaran, BindingResult errors){
        if(errors.hasErrors()){
            return "/tahunajaran/list";
        }
        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
        if (ta != null) {
            ta.setAktif(Status.NONAKTIF);
            tahunAjaranDao.save(ta);
            logger.info("Update tahun ajaran aktif menjadi tidak aktif.");
        }

        tahunAjaranDao.save(tahunAjaran);
        return "redirect:list";
    }

    @RequestMapping(value = "/tahunajaran/aktif", method = RequestMethod.POST)
    public String aktifTahunAjaran (@RequestParam(required = false) TahunAjaran id){
        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
        if (ta != null) {
            ta.setAktif(Status.NONAKTIF);
            tahunAjaranDao.save(ta);
        }
        id.setAktif(Status.AKTIF);
        tahunAjaranDao.save(id);

        return "redirect:list";
    }

}
