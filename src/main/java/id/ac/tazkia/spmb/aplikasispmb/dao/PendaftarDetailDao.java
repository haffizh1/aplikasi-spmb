package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PendaftarDetailDao extends PagingAndSortingRepository<PendaftarDetail, String> {

    PendaftarDetail findByPendaftar(Pendaftar pendaftar);

    Page<PendaftarDetail> findByPendaftar(Pendaftar pendaftar, Pageable pageable);

    @Query(value = "select count(*) as jumlah from  (select t.id from tagihan t " +
            "inner join pendaftar p on p.id = t.id_pendaftar\n" +
            "            inner join pendaftar_detail pd on pd.id_pendaftar = p.id " +
            "            inner join leads l on l.id = p.id_leads where t.id_jenisbiaya = 002  and  p.id_tahun = :ta " +
            "            and l.jenjang = 'S1' and t.lunas = true group by t.id_pendaftar ) aa", nativeQuery = true)
    Long countByPendaftarTahunAjaran(TahunAjaran ta);

    Iterable<PendaftarDetail> findByPendaftarTahunAjaranAktif(Status status);
    Iterable<PendaftarDetail> findByPendaftarTahunAjaranAktifAndPendaftarLeadsJenjang(Status status, Jenjang jenjang);

    Page<PendaftarDetail> findByNimNotNullAndPendaftarLeadsNamaContainingIgnoreCaseOrNimContainingIgnoreCaseAndPendaftarTahunAjaranOrderByNimAsc(String nama,String nim, TahunAjaran tahunAjaran, Pageable pageable);


    Page<PendaftarDetail> findByNimNotNullAndPendaftarTahunAjaran(TahunAjaran ta, Pageable page);

    Iterable<PendaftarDetail> findByNimNotNullAndPendaftarTahunAjaran(TahunAjaran ta);

    Iterable<PendaftarDetail> findByNimNotNullAndPendaftarTahunAjaranAndNim(TahunAjaran ta, String nim);

    PendaftarDetail findByPendaftarIdAndNimNotNull(String id);

    PendaftarDetail findByNim(String nim);
}
