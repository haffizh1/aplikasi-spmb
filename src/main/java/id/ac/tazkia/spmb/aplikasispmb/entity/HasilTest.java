package id.ac.tazkia.spmb.aplikasispmb.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity @Data
public class HasilTest {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @ManyToOne @JoinColumn (name="id_jadwal")
    private JadwalTest jadwalTest;

    @NotNull
    private BigDecimal nilai;

    @ManyToOne @JoinColumn (name="id_grade")
    private Grade grade;

    @ManyToOne @JoinColumn (name="id_periode")
    private Periode periode;

    private String keterangan;

    @ManyToOne @JoinColumn (name="user_insert")
    private User user;

    @NotNull
    private LocalDateTime tanggal_insert = LocalDateTime.now();



}
