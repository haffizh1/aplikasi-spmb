package id.ac.tazkia.spmb.aplikasispmb.dto;

import id.ac.tazkia.spmb.aplikasispmb.entity.Pendaftar;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class EditDuDto {
    private Pendaftar idPendaftar;
    private String tagihan;
    private String idHasilTest;
    private BigDecimal totalTagihan;
    private String keterangan;
    private String cicilan;
    private String konsentrasi;
}
