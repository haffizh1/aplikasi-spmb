package id.ac.tazkia.spmb.aplikasispmb.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.dao.LeadsDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.PendaftarDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.PesertaCourseDao;
import id.ac.tazkia.spmb.aplikasispmb.dto.*;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class NotifikasiService {
    private static final Logger LOGGER = LoggerFactory.getLogger(NotifikasiService.class);

    @Value("${kafka.topic.notifikasi}") private String topicNotifikasi;
    @Value("${notifikasi.registrasi.konfigurasi.it-reset-password}") private String getKonfigurasiNotifikasiReset;
    @Value("${notifikasi.registrasi.konfigurasi.peserta.course}") private String getKonfigurasiNotifikasiCourse;
    @Value("${notifikasi.registrasi.konfigurasi.pmb.referal}") private String getKonfigurasiNotifikasiReferal;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired private ObjectMapper objectMapper;
    @Autowired private PendaftarDao pendaftarDao;
    @Autowired private LeadsDao leadsDao;
    @Autowired private PesertaCourseDao pesertaCourseDao;

    @Async
    public void resetPassword(ResetPassword p) {
        User user = p.getUser();
        Leads leads = leadsDao.findByUser(user);
        Pendaftar pendaftar = pendaftarDao.findByLeads(leads);
        NotifikasiResetPassword notif = NotifikasiResetPassword.builder()
                .konfigurasi(getKonfigurasiNotifikasiReset)
                .email(pendaftar.getLeads().getEmail())
                .data(DataResetPassword.builder()
                        .code(p.getCode())
                        .nama(p.getUser().getUsername())
                        .build())
                .build();

        try {
            kafkaTemplate.send(topicNotifikasi, objectMapper.writeValueAsString(notif));
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
        }
    }

    @Async
    public void pesertaCourse(PesertaCourseDto p) {
        NotifikasiResetPassword notif = NotifikasiResetPassword.builder()
                .konfigurasi(getKonfigurasiNotifikasiCourse)
                .email(p.getEmail())
                .data(DataCourse.builder()
                        .nomorPeserta(p.getNomor())
                        .nama(p.getNama())
                        .noHp(p.getNoHp())
                        .email(p.getEmail())
                        .username(p.getNomor())
                        .password(p.getNomor())
                        .build())
                .build();

        try {
            kafkaTemplate.send(topicNotifikasi, objectMapper.writeValueAsString(notif));
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
        }
    }


    @Async
    public void NotifikasiReferal(NotifRegistrasiReferalDto p) {
        NotifikasiResetPassword notif = NotifikasiResetPassword.builder()
                .konfigurasi(getKonfigurasiNotifikasiReferal)
                .email(p.getEmailRef())
                .data(DataRegistrasiReferal.builder()
                        .nomor(p.getNomor())
                        .nama(p.getNama())
                        .noHp(p.getNoHp())
                        .email(p.getEmail())
                        .kodeRef(p.getKodeRef())
                        .emailRef(p.getEmailRef())
                        .build())
                .build();

        try {
            kafkaTemplate.send(topicNotifikasi, objectMapper.writeValueAsString(notif));
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
        }
    }

}
