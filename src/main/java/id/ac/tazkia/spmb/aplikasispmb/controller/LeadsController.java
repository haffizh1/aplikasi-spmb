package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dto.LeadsDto;
import id.ac.tazkia.spmb.aplikasispmb.dto.UploadError;
import id.ac.tazkia.spmb.aplikasispmb.dto.ValidasiTagihan;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import id.ac.tazkia.spmb.aplikasispmb.service.LeadsService;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.parameters.P;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Controller
public class LeadsController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LeadsController.class);

    @Autowired
    private LeadsDao leadsDao;
    @Autowired
    private TahunAjaranDao tahunAjaranDao;
    @Autowired
    private LeadsService leadsService;
    @Autowired
    private UserDao userDao;
    @Autowired
    private UserPasswordDao userPasswordDao;
    @Autowired
    private PendaftarDao pendaftarDao;
    @Autowired
    private ReferalDao referalDao;

    @GetMapping("/api/username")
    @ResponseBody
    public String findByName(@RequestParam(required = false) String username){
        User user = userDao.findByUsername(username);
        if (user == null) {
            return "Username dapat digunakan";
        } else {
            return "Username sudah digunakan, silahkan ganti";
        }

    }

    //list
    @RequestMapping("/leads/list")
    public void listLeads(@RequestParam(required = false)String nama,
                          @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate tglAwal,
                          @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate tglAkhir,
                          Model m, Pageable page) {
        if (tglAwal != null && tglAkhir != null && nama != null) {
            TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
            Page<Leads> leadsPage = leadsDao.findByTahunAjaranAndNamaContainingIgnoreCaseAndCreateDateBetweenOrderByNama(ta, nama,
                    tglAwal.atStartOfDay(), tglAkhir.plusDays(1).atStartOfDay(), page);

            m.addAttribute("nama", nama);
            m.addAttribute("tglAwal", tglAwal);
            m.addAttribute("tglAkhir", tglAkhir);
            m.addAttribute("daftarLeads", leadsPage);


            List<ValidasiTagihan> leads = new ArrayList<>();
            for (Leads lds : leadsPage) {
                Pendaftar pendaftar = pendaftarDao.findByLeads(lds);
                ValidasiTagihan validasiLeads = new ValidasiTagihan();
                validasiLeads.setPendaftar(lds.getId());
                if (pendaftar != null) {
                    validasiLeads.setStatus("Ada");
                }
                if (pendaftar == null) {
                    validasiLeads.setStatus("Kosong");
                }
                leads.add(validasiLeads);

            }
            m.addAttribute("cekPendaftar", leads);

        }else if (tglAwal == null && tglAkhir == null && nama != null){
            TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
            Page<Leads> leadsPage = leadsDao.findByTahunAjaranAndNamaContainingIgnoreCaseOrderByNama(ta, nama, page);

            m.addAttribute("nama", nama);
            m.addAttribute("daftarLeads", leadsPage);

            List<ValidasiTagihan> leads = new ArrayList<>();
            for (Leads lds : leadsPage) {
                Pendaftar pendaftar = pendaftarDao.findByLeads(lds);
                ValidasiTagihan validasiLeads = new ValidasiTagihan();
                validasiLeads.setPendaftar(lds.getId());
                if (pendaftar != null) {
                    validasiLeads.setStatus("Ada");
                }
                if (pendaftar == null) {
                    validasiLeads.setStatus("Kosong");
                }
                leads.add(validasiLeads);

            }
            m.addAttribute("cekPendaftar", leads);
        }else if(tglAwal != null && tglAkhir != null && nama == null){
            TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
            Page<Leads> leadsPage = leadsDao.findByTahunAjaranAndCreateDateBetweenOrderByNama(ta, tglAwal.atStartOfDay(), tglAkhir.plusDays(1).atStartOfDay(), page);

            m.addAttribute("tglAwal", tglAwal);
            m.addAttribute("tglAkhir", tglAkhir);
            m.addAttribute("daftarLeads", leadsPage);

            List<ValidasiTagihan> leads = new ArrayList<>();
            for (Leads lds : leadsPage) {
                Pendaftar pendaftar = pendaftarDao.findByLeads(lds);
                ValidasiTagihan validasiLeads = new ValidasiTagihan();
                validasiLeads.setPendaftar(lds.getId());
                if (pendaftar != null) {
                    validasiLeads.setStatus("Ada");
                }
                if (pendaftar == null) {
                    validasiLeads.setStatus("Kosong");
                }
                leads.add(validasiLeads);

            }
            m.addAttribute("cekPendaftar", leads);
        }else {
            Page<Leads> leadsPage = leadsDao.findByTahunAjaranAktif(Status.AKTIF,page);
            m.addAttribute("daftarLeads",leadsPage);

            Page<Leads> ps = leadsDao.findByTahunAjaranAktif(Status.AKTIF,page);

            List<ValidasiTagihan> leads = new ArrayList<>();
            for (Leads lds : ps) {
                Pendaftar pendaftar = pendaftarDao.findByLeads(lds);
                ValidasiTagihan validasiLeads = new ValidasiTagihan();
                validasiLeads.setPendaftar(lds.getId());
                if (pendaftar != null){
                    validasiLeads.setStatus("Ada");
                }
                if (pendaftar == null){
                    validasiLeads.setStatus("Kosong");
                }
                leads.add(validasiLeads);

            }
            m.addAttribute("cekPendaftar", leads);
        }
    }
//

    @GetMapping("/leads/form")
    public String  formLeads(@RequestParam(value = "id", required = false) String id,String kode,
                          Model m) {
        //defaultnya, isi dengan object baru
        m.addAttribute("leads", new Leads());

        if (id != null && !id.isEmpty()) {
            Leads leads = leadsDao.findById(id).get();
            if (leads != null) {
                m.addAttribute("leads", leads);
            }
        }

        if (kode != null){
            Referal cekReferal = referalDao.findAllByKodeReferal(kode);
            if (cekReferal != null) {
                LOGGER.info("Referal {} ditemukan", kode);
                m.addAttribute("kodeReferal", kode);
            }
        }
        return "leads/form";
    }

    @PostMapping("/leads/form")
    public String prosesLeads(@Valid LeadsDto leadsDto, RedirectAttributes redirectAttributes){
        User cekUser = userDao.findByUsername(leadsDto.getEmail());

        if (cekUser == null) {
            leadsService.registrasiLeads(leadsDto);
            return "redirect:/leads/selesai";
        }else{
            redirectAttributes.addFlashAttribute("email", leadsDto.getEmail());
            System.out.println(leadsDto.getEmail());
            return "redirect:/leads/gagal";
        }

    }

    @PostMapping("/leads/hapus")
    public String deleteLeads(@RequestParam(value = "id", required = true)String id, String nama){
        User user = userDao.findById(id).get();
        UserPassword userPassword = userPasswordDao.findByUser(user);
        UserPassword userPDelete = userPasswordDao.findById(userPassword.getId()).get();
        Leads le  = leadsDao.findByUser(user);
        Leads leDelete = leadsDao.findById(le.getId()).get();
        nama = le.getNama();

        leadsDao.delete(leDelete);
        userPasswordDao.delete(userPDelete);
        userDao.delete(user);
        return "redirect:/leads/list?nama="+ nama;
    }

    @Autowired
    private PasswordEncoder passwordEncoder;
    @PostMapping("/leads/reset")
    public String resetPassword(@RequestParam(value = "id", required = true)String id){
        User user = userDao.findById(id).get();
        UserPassword userPassword = userPasswordDao.findByUser(user);

        String password = "123";
        userPassword.setPassword(passwordEncoder.encode(password));

        userPasswordDao.save(userPassword);
        return "redirect:/leads/list";
    }

    @RequestMapping(value = "/leads/update", method = RequestMethod.GET)
    public String leadsUpdateForm(@RequestParam(value = "id", required = false) String id,
                             Model m){
        //defaultnya, isi dengan object baru
        m.addAttribute("leads", new Leads());

        if (id != null && !id.isEmpty()){
            Leads leads= leadsDao.findById(id).get();
            if (leads != null){
                m.addAttribute("leads", leads);
            }

        }
        return "leads/update";
    }

    @RequestMapping(value = "/leads/update", method = RequestMethod.POST)
    public String prosesForm(@Valid Leads l, BindingResult errors){
        if(errors.hasErrors()){
            return "leads/update";
        }
        leadsDao.save(l);
        return "redirect:list";
    }

    @GetMapping("/leads/selesai")
    public void selesai(){}

    @GetMapping("/leads/gagal")
    public void gagal(){}

    @GetMapping("/leads/xlsx")
    public void rekapHumasXlsx(HttpServletResponse response) throws Exception {

        String[] columns = {"No","Nama","Email","No Telephone","Jenjang","Username","Status","Tanggal Buat" };

        Iterable<Leads> dataPendaftar = leadsDao.findByTahunAjaranAktif(Status.AKTIF);

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Leads 2020");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1 ;
        int baris = 1 ;

        for (Leads p : dataPendaftar) {
            Iterable<Pendaftar> pendaftar = pendaftarDao.findByLeadsId(p.getId());
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(p.getNama());
            row.createCell(2).setCellValue(p.getEmail());
            row.createCell(3).setCellValue(p.getNoHp());
            row.createCell(4).setCellValue(p.getJenjang().toString());
            row.createCell(5).setCellValue(p.getUser().getUsername());
            if (pendaftar !=  null) {
                for (Pendaftar pendaftars : pendaftar) {
                    if (p.getId() == pendaftars.getLeads().getId()) {
                        row.createCell(6).setCellValue("Sudah Daftar");
                    }
                }
            }
            row.createCell(6).setCellValue(p.getCreateDate().toString());
        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }



        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=leads-"+ LocalDate.now() +".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();

    }
//link Iklan Institut

    @GetMapping ("/institut")
    public String formInstitut (@RequestParam(value = "id", required = false) String id,
                              Model m) {
        //defaultnya, isi dengan object baru
        m.addAttribute("leads", new Leads());

        if (id != null && !id.isEmpty()) {
            Leads leads = leadsDao.findById(id).get();
            if (leads != null) {
                m.addAttribute("leads", leads);
            }
        }
        return "institut";
    }

    @PostMapping("/institut")
    public String  poroses(@Valid LeadsDto leadsDto, BindingResult errors){
        User cekUser = userDao.findByUsername(leadsDto.getUsername());

        if (cekUser == null) {
            leadsService.registrasiLeads(leadsDto);
            return "redirect:/succes";
        }else{
            return "redirect:/fail";
        }
    }

    @GetMapping("/succes")
    public void formSucces(){
    }
    @GetMapping("/fail")
    public void formFail(){
    }
}
