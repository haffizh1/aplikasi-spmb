package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.JadwalTest;
import id.ac.tazkia.spmb.aplikasispmb.entity.JenisTest;
import id.ac.tazkia.spmb.aplikasispmb.entity.Pendaftar;
import id.ac.tazkia.spmb.aplikasispmb.entity.TahunAjaran;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;

public interface JadwalTestDao extends PagingAndSortingRepository<JadwalTest, String> {
    JadwalTest findByPendaftar(Pendaftar pendaftar);

    @Query ("select j from JadwalTest j where j.pendaftar.leads.nama like %:nama% and j.pendaftar.tahunAjaran = :tahun and j.jenisTest != :jenis order by j.tanggalTest DESC")
    Page<JadwalTest> findJadwalTest (@Param("nama") String nama,  @Param("tahun") TahunAjaran tahun,@Param("jenis")JenisTest jenisTest, Pageable page);

    @Query ("select  j from JadwalTest  j where j.pendaftar.tahunAjaran = :tahun and j.jenisTest != :jenis order by j.tanggalTest DESC")
    Page<JadwalTest> findJadwalTestAll (@Param("tahun") TahunAjaran tahun,@Param("jenis")JenisTest jenisTest,Pageable page);

    @Query ("select j from JadwalTest j where j.pendaftar.leads.nama like %:smart% and month(j.tanggalTest) = :bulan and j.pendaftar.tahunAjaran = :tahun and j.jenisTest = :jenis")
    Page<JadwalTest> findJadwalTestSt (@Param("smart") String smart, @Param("bulan") Integer bulan, @Param("tahun") TahunAjaran tahun,@Param("jenis")JenisTest jenisTest, Pageable page);

    @Query ("select  j from JadwalTest  j where month(j.tanggalTest) = :bulan and j.pendaftar.tahunAjaran = :tahun and j.jenisTest = :jenis")
    Page<JadwalTest> findJadwalTestSmart (@Param("bulan") Integer bulan, @Param("tahun") TahunAjaran tahun, @Param("jenis")JenisTest jenisTest, Pageable page);







}
