package id.ac.tazkia.spmb.aplikasispmb.dto;

import lombok.Data;

@Data
public class GetJadwalMatkul {
    private String id;
    private String namaProdi;
    private String namaKelas;
    private String kodeMatakuliah;
    private String namaMatakuliah;
    private String namaMatakuliahEnglish;
    private String idDosen;
    private String dosen;
    private String jamMulai;
    private String jamSelesai;
    private String idNumberElearning;
    private String idTahunAkademik;
    private String status;
}
