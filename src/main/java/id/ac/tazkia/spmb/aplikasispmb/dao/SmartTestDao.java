package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.SmartTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface SmartTestDao extends PagingAndSortingRepository<SmartTest, String> {
    Page<SmartTest> findByAsalSekolahContainingIgnoreCase(String asalSekolah, Pageable pageable);

    @Query("select s from SmartTest s where s.nama like %:nama% ")
    Page<SmartTest> findByNamaSt (@Param("nama")String nama, Pageable pageable);

    Page<SmartTest> findByNamaContainingIgnoreCaseAndAsalSekolahContainingIgnoreCase(String nama, String sekolah, Pageable pageable);
    Page<SmartTest> findByNamaContainingIgnoreCaseOrAsalSekolahContainingIgnoreCase(String nama, String sekolah, Pageable pageable);




}
