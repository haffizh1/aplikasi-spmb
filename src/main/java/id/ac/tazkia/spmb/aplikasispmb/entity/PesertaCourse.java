package id.ac.tazkia.spmb.aplikasispmb.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity @Data
public class PesertaCourse {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private String nomor;

    @NotNull
    private String nama;

    @NotNull
    private String email;

    @NotNull
    private String noHp;


    @NotNull
    @ManyToOne
    @JoinColumn(name="id_course")
    private Course course;

    private String kelas;

    @NotNull
    private String pembayaran;

    @NotNull
    private String status;

}
