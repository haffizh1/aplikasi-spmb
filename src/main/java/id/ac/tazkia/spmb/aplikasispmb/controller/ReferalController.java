package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.dao.ProgramStudiDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.ReferalDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.UserDao;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;

@Controller
public class ReferalController {
    private static final Logger logger = LoggerFactory.getLogger(ReferalController.class);


    @Autowired private ReferalDao referalDao;
    @Autowired private UserDao userDao;
    @Autowired private ProgramStudiDao programStudiDao;

    @GetMapping("/referal/list")
    public void listReferal(@RequestParam(required = false)String keterangan, Model m, Pageable page) {
        if (StringUtils.hasText(keterangan)) {
            m.addAttribute("keterangan", keterangan);
            m.addAttribute("daftarReferal", referalDao.findByNamaContainingIgnoreCaseOrKodeReferalOrderByNama(keterangan, keterangan, page));
        } else {
            m.addAttribute("daftarReferal", referalDao.findAll(page));
        }
    }

    @ModelAttribute("daftarProdi")
    public Iterable<ProgramStudi> daftarProdi(){
        String id ="000";
        String dua = "999";

        return programStudiDao.cariProdi(id, dua);
    }

    @GetMapping("/referal/form")
    public String  referalForm(@RequestParam(value = "id", required = false) String id,
                            Model m){
        //defaultnya, isi dengan object baru
        m.addAttribute("referal", new Referal());

        if (id != null && !id.isEmpty()){
            Referal p= referalDao.findById(id).get();
            if (p != null){
                m.addAttribute("referal", p);
            }
        }
        return "referal/form";
    }


    @PostMapping("/referal/form")
    public String prosesForm(@Valid Referal p, BindingResult errors, Authentication currentUser){
        logger.debug("Authentication class : {}",currentUser.getClass().getName());

        if(currentUser == null){
            logger.warn("Current user is null");
        }

        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

        p.setUserInsert(u);
        p.setTanggalInsert(LocalDateTime.now());
        referalDao.save(p);
        return "redirect:/referal/list";
    }


    @RequestMapping(value = "/referal/aktif", method = RequestMethod.POST)
    public String aktifReferal (@RequestParam(required = false) Referal id){

        id.setStatus(Boolean.TRUE);
        referalDao.save(id);
        return "redirect:list";
    }

    @RequestMapping(value = "/referal/nonaktif", method = RequestMethod.POST)
    public String NonAktifReferal (@RequestParam(required = false) Referal id){

        id.setStatus(Boolean.FALSE);
        referalDao.save(id);
        return "redirect:list";
    }
}
