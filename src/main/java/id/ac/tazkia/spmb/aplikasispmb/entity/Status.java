package id.ac.tazkia.spmb.aplikasispmb.entity;

public enum Status {
    AKTIF,NONAKTIF,WAITING,APPROVED,REJECTED
}
