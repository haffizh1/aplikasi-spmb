package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;

public interface LeadsDao extends PagingAndSortingRepository<Leads, String> {
    Leads findByUser (User u);

    Page<Leads> findByTahunAjaranAndNamaContainingIgnoreCaseAndCreateDateBetweenOrderByNama(TahunAjaran tahunAjaran, String nama, LocalDateTime localDateTime,
                                                                                            LocalDateTime localDateTime1, Pageable pageable);
    Page<Leads> findByTahunAjaranAndCreateDateBetweenOrderByNama(TahunAjaran tahunAjaran, LocalDateTime localDateTime,
                                                                                            LocalDateTime localDateTime1, Pageable pageable);
    Page<Leads> findByTahunAjaranAndNamaContainingIgnoreCaseOrderByNama(TahunAjaran tahunAjaran, String nama, Pageable pageable);

    Page<Leads> findByTahunAjaranAktif(Status status, Pageable page);
    Iterable<Leads> findByTahunAjaranAktif(Status status);

    @Query("select p from Leads p where (lower(p.user.username) = :username)")
    Leads cariUsername(@Param("username") String username);

    Long countLeadsByTahunAjaranAndJenjang(TahunAjaran th, Jenjang j);
}
