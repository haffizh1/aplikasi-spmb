package id.ac.tazkia.spmb.aplikasispmb.dto;

import lombok.Data;

@Data
public class SubscriberRequest {

    private String email;
    private String name;
    private Boolean resubcribe = false;
    private String type;
}
