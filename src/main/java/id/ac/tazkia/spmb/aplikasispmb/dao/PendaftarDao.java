package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;

public interface PendaftarDao extends PagingAndSortingRepository <Pendaftar, String> {
    Pendaftar findByLeads(Leads leads);

    Page<Pendaftar> findByNomorRegistrasiContainingIgnoreCaseOrLeadsNamaContainingIgnoreCaseAndTahunAjaranOrderByNomorRegistrasi(String nomorRegistrasi,String nama, TahunAjaran tahunAjaran, Pageable pageable);
    Page<Pendaftar> findByNomorRegistrasiContainingIgnoreCaseOrLeadsNamaContainingIgnoreCaseAndTahunAjaranAndLeadsJenjangOrderByNomorRegistrasi(String nomorRegistrasi,String nama, TahunAjaran tahunAjaran,Jenjang jenjang, Pageable pageable);

    Page<Pendaftar> findByTahunAjaranAktif(Status status, Pageable page);
    Page<Pendaftar> findByTahunAjaranAktifAndLeadsJenjang(Status status,Jenjang jenjang, Pageable page);
    Iterable<Pendaftar> findByTahunAjaranAktifAndLeadsJenjang(Status status,Jenjang jenjang);

    Pendaftar findByNomorRegistrasi(String nomor);

    Iterable<Pendaftar> findByTahunAjaranAktif(Status status);

    Long countPendaftarByLeadsJenjangNotAndTahunAjaran(Jenjang jenjang,TahunAjaran tahun);


    Long countPendaftarByProgramStudiAndTahunAjaran(ProgramStudi programStudi, TahunAjaran tahun);
    Long countPendaftarByTahunAjaran(TahunAjaran tahun);

    Long countPendaftarByLeadsJenjangAndTahunAjaran(Jenjang jenjang, TahunAjaran tahun);


    Iterable<Pendaftar> findByLeadsId (String leads);
}
