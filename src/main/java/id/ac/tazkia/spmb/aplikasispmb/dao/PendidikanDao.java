package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.Pendidikan;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PendidikanDao extends PagingAndSortingRepository<Pendidikan, String> {

}
