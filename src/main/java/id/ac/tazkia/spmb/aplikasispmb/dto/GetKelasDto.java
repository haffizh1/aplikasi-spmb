package id.ac.tazkia.spmb.aplikasispmb.dto;

import lombok.Data;

@Data
public class GetKelasDto {
    private String idKelas;
    private String kodeKelas;
    private String namaKelas;
    private String keterangan;
    private String idProdi;
    private String status;
    private String kurikulum;
    private String konsentrasi;
    private String angkatan;
    private String bahasa;
}
