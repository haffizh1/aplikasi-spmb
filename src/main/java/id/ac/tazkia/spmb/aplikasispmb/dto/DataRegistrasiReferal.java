package id.ac.tazkia.spmb.aplikasispmb.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DataRegistrasiReferal {
    private String nomor;
    private String nama;
    private String email;
    private String noHp;
    private String emailRef;
    private String kodeRef;
}
