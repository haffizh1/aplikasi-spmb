package id.ac.tazkia.spmb.aplikasispmb.dto;

import lombok.Data;

@Data
public class PesertaCourseDto {
    private String nomor;
    private String nama;
    private String email;
    private String noHp;
    private String idElearning;
    private String status;
}
