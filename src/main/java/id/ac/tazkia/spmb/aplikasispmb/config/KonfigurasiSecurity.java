package id.ac.tazkia.spmb.aplikasispmb.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class KonfigurasiSecurity extends WebSecurityConfigurerAdapter {
    private static final String SQL_LOGIN
            = "select u.username as username,p.password as password, active\n"
            + "FROM s_user u\n"
            + "inner join s_user_password p on p.id_user = u.id\n"
            + "WHERE u.username = ?";

    private static final String SQL_PERMISSION
            = "select u.username, p.permission_value as authority "
            + "from s_user u "
            + "inner join s_role r on u.id_role = r.id "
            + "inner join s_role_permission rp on rp.id_role = r.id "
            + "inner join s_permission p on rp.id_permission = p.id "
            + "where u.username = ?";

    @Value("${bcrypt.strength}")
    private Integer bcryptStrength;
    @Autowired
    private DataSource ds;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
        auth
                .jdbcAuthentication()
                .dataSource(ds)
                .usersByUsernameQuery(SQL_LOGIN)
                .authoritiesByUsernameQuery(SQL_PERMISSION)
                .passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/pendaftar/list").hasAnyAuthority("VIEW_MASTER","VIEW_FINANCE","VIEW_AKADEMIK")
                .antMatchers("/pendaftar/list").hasAnyAuthority("VIEW_MASTER","VIEW_FINANCE","VIEW_AKADEMIK")
                .anyRequest().authenticated()
                .and().logout().permitAll()
                .and().formLogin().defaultSuccessUrl("/dashboard", true)
                .loginPage("/login")
                .permitAll();

    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/suratKeterangan")
                .antMatchers("/brosur/pei")
                .antMatchers("/brosur/mua")
                .antMatchers("/brosur/ai")
                .antMatchers("/brosur/bmi")
                .antMatchers("/brosur/ei")
                .antMatchers("/brosur/institut")
                .antMatchers("/brosur/d3")
                .antMatchers("/brosur/mes")
                .antMatchers("/brosur/mas")
                .antMatchers("/leads/form")
                .antMatchers("/leads/selesai")
                .antMatchers("/leads/gagal")
                .antMatchers("/api/username")
                .antMatchers("/favicon.ico")
                .antMatchers("/info")
                .antMatchers("/js/**")
                .antMatchers("../img/*")
                .antMatchers("/images/**")
                .antMatchers("/index/**")
                .antMatchers("/frontend")
                .antMatchers("/reset")
                .antMatchers("/reset_sukses")
                .antMatchers("/reset_gagal")
                .antMatchers("/confirm")
                .antMatchers("/institut")
                .antMatchers("/succes")
                .antMatchers("/fail")
                .antMatchers("/")
                .antMatchers("/mbs")
                .antMatchers("/as")
                .antMatchers("/hes")
                .antMatchers("/kpi")
                .antMatchers("/smartTest/listIndex")
                .antMatchers("/panduanPembayaran")
                .antMatchers("/api/mahasiswanim")
                .antMatchers("/api/program-studi")
                .antMatchers("/api/input-leads")
                .antMatchers("/api/kabupaten")
                .antMatchers("/api/getPesertaCourse")
                .antMatchers("/sitinprogram")
                .antMatchers("/sitin_sukses")
                .antMatchers("/sitinprogram-detail")
                .antMatchers("/fotoCover/{course}/cover/")
                .antMatchers("/fotoDosen/{dosen}/foto/")
                .antMatchers("/css/**")
                .antMatchers("/lib/**") ;

    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}

