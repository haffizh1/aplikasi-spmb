package id.ac.tazkia.spmb.aplikasispmb.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity @Getter
@Setter
public class Pendaftar {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @OneToOne
    @JoinColumn(name = "id_leads")
    private Leads leads;

    @NotNull
    @NotEmpty
    private String nomorRegistrasi;

    @ManyToOne
    @JoinColumn(name = "id_program_studi")
    private ProgramStudi programStudi;

    @Column(nullable = false)
    private String konsentrasi;

    @ManyToOne @JoinColumn(name = "kota_asal_sekolah")
    private KabupatenKota idKabupatenKota;

    @Column(nullable = false)
    @NotNull
    @NotEmpty
    private String namaAsalSekolah;

    @Column(nullable = false)
    private String perekomendasi;

    @Column(nullable = false)
    private String namaPerekomendasi;

    @OneToOne
    @JoinColumn(name = "id_tahun")
    private TahunAjaran tahunAjaran;

    @OneToOne
    @JoinColumn(name = "id_referal")
    private Referal referal;

    @OneToOne(mappedBy = "pendaftar")
    @JsonBackReference
    private PendaftarDetail pendaftarDetail;

    @OneToOne(mappedBy = "pendaftar")
    @JsonBackReference
    private JadwalTest jadwalTest;

    @Column(columnDefinition="LONGTEXT")
    private String alasanMemilihProdi;

}
