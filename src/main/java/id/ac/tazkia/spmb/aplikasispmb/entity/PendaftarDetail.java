package id.ac.tazkia.spmb.aplikasispmb.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity @Getter @Setter
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class PendaftarDetail {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_pendaftar")
    private Pendaftar pendaftar;

    private String foto;

    @Column(nullable = false)
    @NotNull
    @NotEmpty
    private String tempatLahir;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull
    @Column(columnDefinition = "DATE")
    private LocalDate tanggalLahir;

    @NotNull
    @NotEmpty
    private String agama;

    @NotNull
    @NotEmpty
    private String jenisKelamin;

    @NotNull
    @NotEmpty
    @Size(max = 2)
    private String golonganDarah;

    @NotNull
    @NotEmpty
    private String noKtp;

    @NotNull
    @NotEmpty
    private String alamatRumah;

    @NotNull
    @NotEmpty
    private String negara;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_provinsi")
    private Provinsi provinsi;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_kokab")
    private KabupatenKota kokab;

    @NotNull
    @NotEmpty
    private String kodePos;

    private String nisn;

    @NotNull
    @NotEmpty
    private String jurusanSekolah;

    @NotNull
    @NotEmpty
    private String tahunLulus;

    @NotNull
    @NotEmpty
    private String statusSipil;

    @NotNull
    @NotEmpty
    private String rencanaBiaya;

    private String nim;

    @NotNull
    private LocalDate updateTime = LocalDate.now();


}
