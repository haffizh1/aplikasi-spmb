package id.ac.tazkia.spmb.aplikasispmb.entity;

public enum CaraPembayaran {
    TUNAI, TRANSFER, VIRTUAL_ACCOUNT, BEASISWA
}
