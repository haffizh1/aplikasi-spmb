package id.ac.tazkia.spmb.aplikasispmb.entity;

public enum JenisTest {
    SMART_TEST,
    JPA,
    TPA
}
