package id.ac.tazkia.spmb.aplikasispmb.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class HapusTagihanRequest {

    private String jenisTagihan;
    private String kodeBiaya;
    private String debitur;
    private String nomorTagihan;

}
