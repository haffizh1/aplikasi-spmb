package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PendaftarOrangTuaDao extends PagingAndSortingRepository<PendaftarOrangtua, String> {
    PendaftarOrangtua findByPendaftar(Pendaftar pendaftar);

    Iterable<PendaftarOrangtua> findByPendaftarTahunAjaranAktif(Status aktif);
    Iterable<PendaftarOrangtua> findByPendaftarTahunAjaranAktifAndPendaftarLeadsJenjang(Status aktif, Jenjang jenjang);
    Iterable<PendaftarOrangtua> findByPendaftarTahunAjaranAndPendaftar(TahunAjaran ta, Pendaftar pendaftar);

    Iterable<PendaftarOrangtua> findByPendaftarTahunAjaran(TahunAjaran ta);
}
