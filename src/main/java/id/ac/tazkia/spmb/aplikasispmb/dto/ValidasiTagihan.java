package id.ac.tazkia.spmb.aplikasispmb.dto;

import lombok.Data;

@Data
public class ValidasiTagihan {
    private String pendaftar;
    private String status;
}
