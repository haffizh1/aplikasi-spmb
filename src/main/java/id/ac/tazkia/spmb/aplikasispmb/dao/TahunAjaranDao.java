package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.Status;
import id.ac.tazkia.spmb.aplikasispmb.entity.TahunAjaran;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TahunAjaranDao extends PagingAndSortingRepository<TahunAjaran, String> {
    TahunAjaran findByAktif(Status status);
}
