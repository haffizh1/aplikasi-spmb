package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.JenisBiaya;
import id.ac.tazkia.spmb.aplikasispmb.entity.Pendaftar;
import id.ac.tazkia.spmb.aplikasispmb.entity.Tagihan;
import id.ac.tazkia.spmb.aplikasispmb.entity.TahunAjaran;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface TagihanDao extends PagingAndSortingRepository<Tagihan, String> {
    Long countByPendaftar(Pendaftar pendaftar);

    Page<Tagihan> findByPendaftarAndJenisBiaya(Pendaftar  pendaftar, JenisBiaya jenisBiaya, Pageable pageable);

    List<Tagihan> findByPendaftarOrderByTanggalTagihan(Pendaftar pendaftar, Pageable page);

    Page<Tagihan> findById(String tagihan, Pageable page);

    Tagihan findByPendaftar(Pendaftar p);

    Tagihan findByNomorTagihan(String nomor);

    Tagihan findByPendaftarAndJenisBiaya(Pendaftar pendaftar, JenisBiaya jenisBiaya);
    Tagihan findByPendaftarAndJenisBiayaAndLunas(Pendaftar pendaftar, JenisBiaya jenisBiaya, Boolean lunas);


    Page<Tagihan> findByPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarTahunAjaranAndJenisBiaya(String nama, TahunAjaran tahunAjaran, JenisBiaya  jenisBiaya, Pageable pageable);
    Page<Tagihan> findByPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarTahunAjaran(String nama,TahunAjaran tahunAjaran, Pageable pageable);
    Page<Tagihan> findByJenisBiayaAndPendaftarTahunAjaran(JenisBiaya  jenisBiaya, TahunAjaran tahunAjaran, Pageable pageable);
    Page<Tagihan> findByPendaftarTahunAjaran(TahunAjaran ta, Pageable page);

    @Query(value = "select * from tagihan where id_pendaftar = ?1 and id_jenisbiaya = ?2 and lunas= true limit 1", nativeQuery = true)
    Tagihan cekPendaftarDanJenisBiaya(String pendaftar, String jenisBiaya);


    @Query(value = "select * from tagihan where tanggal_jatuh_tempo < date(now()) and lunas = 0 and id_pendaftar = ?1 and id_jenisbiaya = ?2", nativeQuery = true)
    Iterable<Tagihan> cekTagihan(String pendaftar, String jenisBiaya);

    @Query(value = "select * from tagihan where lunas = 1 and cicilan != 0 and id_jenisbiaya = ?1", nativeQuery = true)
    Iterable<Tagihan> cekTagianCicilan(JenisBiaya jenisBiaya);

    Page<Tagihan> findByPendaftarIdAndJenisBiayaId(String p,String idJb, Pageable pageable);
    Tagihan findByPendaftarIdAndJenisBiayaId(String p,String idJb);

    @Query(value = "select sum(nilai) from tagihan where lunas = :lunas and id_jenisbiaya = 002 and id_pendaftar = :idPendaftar", nativeQuery = true)
    BigDecimal totalBayar(@Param("lunas")Boolean lunas,@Param("idPendaftar")Pendaftar pendaftar);

    Long countTagihanByPendaftarIdAndJenisBiaya(String pendaftar, JenisBiaya jenisBiaya);
}
