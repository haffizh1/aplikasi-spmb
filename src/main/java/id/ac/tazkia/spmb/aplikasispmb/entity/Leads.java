package id.ac.tazkia.spmb.aplikasispmb.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Leads {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull @NotEmpty
    private String nama;

    @NotNull @NotEmpty
    private String email;

    @NotNull @NotEmpty
    private String noHp;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Jenjang jenjang;

    @OneToOne
    @JoinColumn(name = "id_user")
    private User user;

    @OneToOne
    @JoinColumn(name = "id_tahun")
    private TahunAjaran tahunAjaran;

    @NotNull
    private LocalDateTime createDate = LocalDateTime.now();

    @OneToOne
    @JoinColumn(name = "id_referal")
    private Referal kode;

}
