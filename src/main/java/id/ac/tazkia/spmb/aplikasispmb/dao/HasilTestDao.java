package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.HasilTest;
import id.ac.tazkia.spmb.aplikasispmb.entity.JadwalTest;
import id.ac.tazkia.spmb.aplikasispmb.entity.Status;
import id.ac.tazkia.spmb.aplikasispmb.entity.TahunAjaran;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface HasilTestDao extends PagingAndSortingRepository<HasilTest, String> {
    HasilTest findByJadwalTest(JadwalTest jadwalTest);

    Page<HasilTest> findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarLeadsNamaContainingIgnoreCase(TahunAjaran ta, String nama, Pageable pageable);
    Page<HasilTest> findAllByJadwalTestPendaftarTahunAjaran(TahunAjaran ta, Pageable pageable);
    Iterable<HasilTest> findAllByJadwalTestPendaftarTahunAjaranAktif(Status ta);

}
