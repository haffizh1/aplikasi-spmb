package id.ac.tazkia.spmb.aplikasispmb.entity;

public enum JenisBerkas {
    IJAZAH,SKL,KTP,NIRM,FOTO,TRANSKRIP,REKOMENDASI
}
