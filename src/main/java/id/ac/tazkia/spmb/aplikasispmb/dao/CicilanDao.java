package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.Cicilan;
import id.ac.tazkia.spmb.aplikasispmb.entity.JenisBiaya;
import id.ac.tazkia.spmb.aplikasispmb.entity.Pendaftar;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;

public interface CicilanDao extends PagingAndSortingRepository<Cicilan,String> {

    @Query("SELECT COUNT(pendaftar)+1 FROM Cicilan  WHERE pendaftar = :idPendaftar")
    Long itungCicilan(@Param("idPendaftar")Pendaftar pendaftar);

    @Query("SELECT SUM(nominal) FROM Cicilan  WHERE pendaftar = :idPendaftar")
    BigDecimal itungNominalCicilan(@Param("idPendaftar")Pendaftar pendaftar);

    @Query("SELECT SUM(nominal) FROM Cicilan  WHERE pendaftar = :idPendaftar and status = :status")
    BigDecimal itungNominalCicilandanStatus(@Param("idPendaftar")Pendaftar pendaftar,
                                            @Param("status")Boolean status);

    Page<Cicilan> findByPendaftarAndStatusOrderByUrutanCicilanAsc(Pendaftar pendaftar,Boolean status, Pageable pageable);

    Iterable<Cicilan> findAllByPendaftar(Pendaftar pendaftar);

    @Query(value = "select * from cicilan where tanggal_kirim = date(now()) and status = 0 and id_pendaftar = ?1", nativeQuery = true)
    Iterable<Cicilan> cekCicilan(Pendaftar pendaftar);


    @Query(value ="select * from cicilan c " +
            "inner join pendaftar  p  on p.id = c.id_pendaftar " +
            "where c.id_pendaftar = ?1 order by c.urutan_cicilan asc ", nativeQuery = true)
    Iterable<Cicilan> cekCicilanTagihan(Pendaftar pendaftar);
}
